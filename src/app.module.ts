import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { LoggerModule, PinoLogger } from 'nestjs-pino';
import { operatorsAliases } from './config/utils';
import { DocumentosModule } from './documentos/documentos.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        pinoHttp: {
          safe: true,
          prettyPrint: configService.get<string>('NODE_ENV') !== 'production',
        },
      }),
      inject: [ConfigService],
    }),
    SequelizeModule.forRootAsync({
      imports: [ConfigModule, LoggerModule],
      useFactory: async (configService: ConfigService, logger: PinoLogger) => {
        return {
          dialect: 'postgres',
          host: 'db',
          port: 5432,
          username: 'postgres',
          password: 'postgres',
          database: 'postgres',

          // dialect: 'postgres',
          // host: 'db', //configService.get<string>('DB_HOST'),
          // port: 5432,
          // username: 'postgres', // configService.get<string>('DB_USERNAME'),
          // password: 'postgres', // configService.get<string>('DB_PASSWORD'),
          // database: 'postgres', // configService.get<string>('DB_DATABASE'),
          // logging: logger.info.bind(logger),
          typeValidation: true,
          // benchmark: true,
          // native: true,
          operatorsAliases,
          autoLoadModels: true,
          synchronize: true, // configService.get<boolean>('DB_SYNC'),
          define: {
            timestamps: true,
            underscored: true,
            version: true,
            schema: 'public', // configService.get<string>('DB_SCHEMA'),
          },
        };
      },
      inject: [PinoLogger],
    }),
    DocumentosModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

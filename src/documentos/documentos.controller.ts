import Aigle from "aigle";

import { PinoLogger } from "nestjs-pino";
import { Controller, Inject } from "@nestjs/common";
import { GrpcMethod } from "@nestjs/microservices";
import { isEmpty, isNil } from "lodash";

import { ICount, IQuery } from "../commons/commons.interface";
import { IDocumentosService } from "./interfaces/documentos.interface";
import { IFindPayload } from "../commons/cursor-pagination.interface";

import { Documento } from "./documentos.model";
import { DocumentosDto } from "./dto/documentos.dto";

const { map } = Aigle;

@Controller()
export class DocumentosController {
  constructor(
    @Inject("DocumentosService") private readonly service: IDocumentosService,
    private readonly logger: PinoLogger
  ) {
    logger.setContext(DocumentosController.name);
  }

  @GrpcMethod("DocumentosService", "create")
  async create(data: DocumentosDto): Promise<Documento> {
    this.logger.info("DocumentosController#create.call %o", data);

    const result: Documento = await this.service.create(data);

    this.logger.info("DocumentosController#create.result %o", result);

    return result;
  }

  @GrpcMethod("DocumentosService", "find")
  async find(query: IQuery): Promise<IFindPayload<Documento>> {
    this.logger.info("DocumentosController#findAll.call %o", query);

    const { results, cursors } = await this.service.find({
      attributes: !isEmpty(query.select)
        ? ["id"].concat(query.select)
        : undefined,
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
      order: !isEmpty(query.orderBy) ? query.orderBy : undefined,
      limit: !isNil(query.limit) ? query.limit : 25,
      before: !isEmpty(query.before) ? query.before : undefined,
      after: !isEmpty(query.after) ? query.after : undefined,
    });

    const result: IFindPayload<Documento> = {
      edges: await map(results, async (comment: Documento) => ({
        node: comment,
        cursor: Buffer.from(JSON.stringify([comment.id])).toString("base64"),
      })),
      pageInfo: {
        startCursor: cursors?.before || "",
        endCursor: cursors?.after || "",
        hasNextPage: cursors?.hasNext || false,
        hasPreviousPage: cursors?.hasPrevious || false,
      },
    };

    this.logger.info("DocumentosController#findAll.result %o", result);

    return result;
  }

  @GrpcMethod("DocumentosService", "findById")
  async findById({ id }): Promise<Documento> {
    this.logger.info("DocumentosController#findById.call %o", id);

    const result: Documento = await this.service.findById(id);

    this.logger.info("DocumentosController#findById.result %o", result);

    if (isEmpty(result)) throw new Error("Record not found.");

    return result;
  }

  @GrpcMethod("DocumentosService", "findOne")
  async findOne(query: IQuery): Promise<Documento> {
    this.logger.info("DocumentosController#findOne.call %o", query);

    const result: Documento = await this.service.findOne({
      attributes: !isEmpty(query.select) ? query.select : undefined,
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
    });

    this.logger.info("DocumentosController#findOne.result %o", result);

    if (isEmpty(result)) throw new Error("Record not found.");

    return result;
  }

  @GrpcMethod("DocumentosService", "count")
  async count(query: IQuery): Promise<ICount> {
    this.logger.info("DocumentosController#count.call %o", query);

    const count: number = await this.service.count({
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
    });

    this.logger.info("DocumentosController#count.result %o", count);

    return { count };
  }

  @GrpcMethod("DocumentosService", "update")
  async update({ id, data }): Promise<Documento> {
    this.logger.info("DocumentosController#update.call %o %o", id, data);

    const result: Documento = await this.service.update(id, data);

    this.logger.info("DocumentosController#update.result %o", result);

    return result;
  }

  @GrpcMethod("DocumentosService", "destroy")
  async destroy(query: IQuery): Promise<ICount> {
    this.logger.info("DocumentosController#destroy.call %o", query);

    const count: number = await this.service.destroy({
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
    });

    this.logger.info("DocumentosController#destroy.result %o", count);

    return { count };
  }
}

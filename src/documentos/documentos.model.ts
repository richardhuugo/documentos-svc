import * as paginate from "sequelize-cursor-pagination";
import { Column, Model, Table, DataType, Index } from "sequelize-typescript";

@Table({
  modelName: "documento",
  tableName: "documentos",
})
export class Documento extends Model<Documento> {
  @Column({
    primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV1,
    comment: "The identifier for the empresa record.",
  })
  id: string;

  @Index("nr_documento")
  @Column({
    type: DataType.TEXT,
    comment: "The document's nr_documento.",
  })
  nr_documento: string;

  @Index("tipo_documento_id")
  @Column({
    type: DataType.TEXT,
    comment: "The document's tipo_documento_id.",
  })
  tipo_documento_id: string;

  @Index("cliente_id")
  @Column({
    type: DataType.TEXT,
    comment: "The users's user_id.",
  })
  cliente_id: string;
}

paginate({
  methodName: "findAndPaginate",
  primaryKeyField: "id",
})(Documento);

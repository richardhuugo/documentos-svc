import { Module } from '@nestjs/common';
import { LoggerModule } from 'nestjs-pino';
import { SequelizeModule } from '@nestjs/sequelize';

import { Documento } from './documentos.model';
import { DocumentosController } from './documentos.controller';
import { DocumentosService } from './documentos.service';

@Module({
  imports: [LoggerModule, SequelizeModule.forFeature([Documento])],
  providers: [{ provide: 'DocumentosService', useClass: DocumentosService }],
  controllers: [DocumentosController],
})
export class DocumentosModule {}

import { PinoLogger } from "nestjs-pino";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";

import { IDocumentosService } from "./interfaces/documentos.interface";
import { Documento } from "./documentos.model";
import { DocumentosDto } from "./dto/documentos.dto";
import {
  IFindAndPaginateOptions,
  IFindAndPaginateResult,
} from "../commons/find-and-paginate.interface";
import { FindOptions } from "sequelize/types";
import { isEmpty } from "lodash";

@Injectable()
export class DocumentosService implements IDocumentosService {
  constructor(
    @InjectModel(Documento) private readonly repo: typeof Documento,
    private readonly logger: PinoLogger
  ) {
    logger.setContext(DocumentosService.name);
  }

  async find(
    query?: IFindAndPaginateOptions
  ): Promise<IFindAndPaginateResult<Documento>> {
    this.logger.info("DocumentosService#findAll.call %o", query);

    const result: IFindAndPaginateResult<Documento> =
      //@ts-ignore
      await this.repo.findAndPaginate({
        ...query,
        raw: true,
        paranoid: false,
      });

    this.logger.info("DocumentosService#findAll.result %o", result);

    return result;
  }

  async findById(id: string): Promise<Documento> {
    this.logger.info("DocumentosService#findById.call %o", id);

    const result: Documento = await this.repo.findByPk(id, {
      raw: true,
    });

    this.logger.info("DocumentosService#findById.result %o", result);

    return result;
  }

  async findOne(query: FindOptions): Promise<Documento> {
    this.logger.info("DocumentosService#findOne.call %o", query);

    const result: Documento = await this.repo.findOne({
      ...query,
      raw: true,
    });

    this.logger.info("DocumentosService#findOne.result %o", result);

    return result;
  }

  async count(query?: FindOptions): Promise<number> {
    this.logger.info("DocumentosService#count.call %o", query);

    const result: number = await this.repo.count(query);

    this.logger.info("DocumentosService#count.result %o", result);

    return result;
  }

  async create(document: DocumentosDto): Promise<Documento> {
    const result: Documento = await this.repo.create(document);
    return result;
  }

  async update(id: string, user: DocumentosDto): Promise<Documento> {
    this.logger.info("DocumentosService#update.call %o", user);

    const record: Documento = await this.repo.findByPk(id);

    if (isEmpty(record)) throw new Error("Record not found.");

    const result: Documento = await record.update(user);

    this.logger.info("DocumentosService#update.result %o", result);

    return result;
  }

  async destroy(query?: FindOptions): Promise<number> {
    this.logger.info("DocumentosService#destroy.call %o", query);

    const result: number = await this.repo.destroy(query);

    this.logger.info("DocumentosService#destroy.result %o", result);

    return result;
  }
}

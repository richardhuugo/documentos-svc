import { FindOptions } from "sequelize/types";
import {
  IFindAndPaginateOptions,
  IFindAndPaginateResult,
} from "../../commons/find-and-paginate.interface";

import { Documento } from "../documentos.model";
import { DocumentosDto } from "../dto/documentos.dto";

export interface IDocumentosService {
  create(comment: DocumentosDto): Promise<Documento>;

  find(
    query?: IFindAndPaginateOptions
  ): Promise<IFindAndPaginateResult<Documento>>;
  findById(id: string): Promise<Documento>;
  findOne(query?: FindOptions): Promise<Documento>;
  count(query?: FindOptions): Promise<number>;
  update(id: string, comment: DocumentosDto): Promise<Documento>;
  destroy(query?: FindOptions): Promise<number>;
}
